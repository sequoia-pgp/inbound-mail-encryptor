use std::fs::File;
use std::io::{self, Read, Write};
use std::path::PathBuf;

use email::{
    self,
    MimeMessage,
    MimeMultipartType,
};
use structopt::StructOpt;

use sequoia_openpgp as openpgp;
use openpgp::{
    Result,
    cert::prelude::*,
    policy::StandardPolicy,
    packet::{
        Key,
        key::Key4,
        signature::SignatureBuilder,
    },
    parse::Parse,
    serialize::{
        Serialize,
        stream::*,
        stream::padding::*,
    },
    types::{
        Curve,
        KeyFlags,
        ReasonForRevocation,
        SignatureType,
    }
};

#[derive(StructOpt)]
#[structopt(about = "Encrypts incoming mail")]
pub enum IME {
    /// Prints version information.
    Version {
    },
    /// Generates a Secret Key.
    GenerateInboundKey {
        /// Write the key to this file.
        name: PathBuf,
        /// The certificate to generate an incoming encryption key
        /// for.
        cert: PathBuf,
    },
    /// Extracts a Certificate from a Secret Key for deployment on the
    /// server.
    Deploy {
        /// Read the key from this file.
        name: PathBuf,
    },
    /// Encrypts an incoming mail.
    EncryptInboundMail {
        /// Read the key from this file.
        name: PathBuf,
    },
}

fn main() -> Result<()> {
    let p = &StandardPolicy::new();

    match IME::from_args() {
        IME::Version {} => {
            println!("Sequoia Inbound Mail Encryption (Sequoia-OpenPGP {})",
                     openpgp::VERSION);
        },

        IME::GenerateInboundKey { name, cert, } => {
            let cert = Cert::from_file(&cert)?;
            let vcert = cert.with_policy(p, None)?;

            let mut ime_key = if name.exists() {
                Cert::from_file(&name)?
            } else {
                CertBuilder::new()
                    .add_userid(format!("IME for {}", cert.fingerprint()))
                    .generate()?.0
            };

            let mut ime_primary_signer = ime_key.primary_key().key().clone()
                .parts_into_secret()?.into_keypair()?;

            // First, revoke any newly-revoked encryption subkeys.
            for sk in vcert.keys().subkeys().for_storage_encryption().revoked(true) {
                if ime_key.with_policy(p, None)?.keys().revoked(false)
                    .key_handle(sk.fingerprint()).count() == 1
                {
                    let reason = sk.self_revocations().get(0).expect("revoked")
                        .reason_for_revocation()
                        .unwrap_or((ReasonForRevocation::Unspecified,
                                    b"Unspecified"));
                    let sig = SubkeyRevocationBuilder::new()
                        .set_reason_for_revocation(reason.0, reason.1)?
                        .build(&mut ime_primary_signer, &ime_key, &sk, None)?;
                    ime_key = ime_key.insert_packets(sig)?;
                }
            }

            // Second, expire any newly-expired encryption subkeys,
            // add new encryption subkeys.
            for sk in vcert.keys().subkeys().for_storage_encryption().revoked(false) {
                if sk.alive().is_ok() {
                    if ime_key.with_policy(p, None)?.keys().revoked(false)
                        .key_handle(sk.fingerprint()).nth(0).map(|k| ! k.alive().is_ok())
                        .unwrap_or(true)
                    {
                        // sk doesn't exist or is not alive in
                        // ime_key.  Add it.
                        let builder =
                            SignatureBuilder::new(SignatureType::SubkeyBinding)
                            .set_key_flags(
                                &KeyFlags::empty().set_storage_encryption())?;
                        let sig =
                            sk.bind(&mut ime_primary_signer, &ime_key, builder)?;
                        ime_key = ime_key.insert_packets(vec![
                            openpgp::Packet::from(
                                sk.key().clone().take_secret().0.parts_into_public()),
                            sig.into(),
                        ])?;
                    }
                } else {
                    if ime_key.with_policy(p, None)?.keys().revoked(false)
                        .key_handle(sk.fingerprint()).nth(0).map(|k| k.alive().is_ok())
                        .unwrap_or(false)
                    {
                        // sk exists exist and is alive in ime_key.
                        // Expire it.
                        let builder =
                            SignatureBuilder::new(SignatureType::SubkeyBinding)
                            .set_key_validity_period(
                                sk.binding_signature().key_validity_period()
                                    .expect("key is expired"))?
                            .set_key_flags(
                                &KeyFlags::empty().set_storage_encryption())?;
                        let sig =
                            sk.bind(&mut ime_primary_signer, &ime_key, builder)?;
                        ime_key = ime_key.insert_packets(sig)?;
                    }
                }
            }

            // Third, make sure that we have at least one usable
            // encryption subkey.
            if ime_key.with_policy(p, None)?.keys().revoked(false)
                .for_storage_encryption().count() == 0
            {
                return Err(anyhow::anyhow!("No usable encryption subkey left"));
            }

            // Fourth, revoke all signing subkeys and strip the secret
            // key material.
            let mut revocations = Vec::new();
            for sk in ime_key.with_policy(p, None)?.keys().subkeys().for_signing().revoked(false) {
                let sig = SubkeyRevocationBuilder::new()
                    .set_reason_for_revocation(ReasonForRevocation::KeyRetired,
                                               b"Rotated signing subkey")?
                    .build(&mut ime_primary_signer, &ime_key, &sk, None)?;
                revocations.push(sig);
            }
            ime_key = ime_key.insert_packets(revocations)?;
            let mut keys = Vec::new();
            for sk in ime_key.with_policy(p, None)?.keys().subkeys().for_signing().secret() {
                keys.push(sk.key().clone().take_secret().0.parts_into_public());
            }
            ime_key = ime_key.insert_packets(keys)?;

            // Finally, add a new signing subkey.
            let subkey: Key<_, _> =
                Key4::generate_ecc(true, Curve::Ed25519)?.into();

            // We need to create a primary key binding signature.
            let mut subkey_signer = subkey.clone().into_keypair().unwrap();
            let sig = subkey.bind(
                &mut ime_primary_signer, &ime_key,
                SignatureBuilder::new(SignatureType::SubkeyBinding)
                    .set_key_flags(&KeyFlags::empty().set_signing())?
                    .set_embedded_signature(
                        SignatureBuilder::new(SignatureType::PrimaryKeyBinding)
                            .sign_primary_key_binding(&mut subkey_signer,
                                                      &ime_key.primary_key(),
                                                      &subkey)?)?)?;
            ime_key = ime_key.insert_packets(vec![
                openpgp::Packet::from(subkey),
                sig.into(),
            ])?;

            // Sanity checks.
            assert_eq!(ime_key.with_policy(p, None)?.keys().secret()
                       .for_storage_encryption().count(), 0);
            assert_eq!(ime_key.with_policy(p, None)?.keys().secret()
                       .subkeys().count(), 1);

            let mut tmp = name.clone().into_os_string();
            tmp.push("~");
            ime_key.as_tsk().serialize(&mut File::create(&tmp)?)?;
            std::fs::rename(tmp, name)?;
        },

        IME::Deploy { name, } => {
            let ime_key = Cert::from_file(&name)?;
            // Sanity checks.
            assert_eq!(ime_key.with_policy(p, None)?.keys().secret()
                       .for_storage_encryption().count(), 0);
            assert_eq!(ime_key.with_policy(p, None)?.keys().secret()
                       .subkeys().count(), 1);
            assert_eq!(ime_key.with_policy(p, None)?.userids().count(), 1);

            use openpgp::armor::{Writer, Kind};
            let mut sink = Writer::with_headers(
                std::io::stdout(), Kind::SecretKey,
                vec![("Comment",
                      ime_key.with_policy(p, None)?.userids().nth(0).unwrap().userid().name()?.unwrap()),
                     ("Comment", "Deploy this to the server".into())])?;
            ime_key.as_tsk()
                .set_filter(|k| k.fingerprint() != ime_key.fingerprint())
                .serialize(&mut sink)?;
            sink.finalize()?;
        },

        IME::EncryptInboundMail { name, } => {
            let ime_key = Cert::from_file(&name)?;
            let valid_ime_key = ime_key.with_policy(p, None)?;
            // Sanity checks. XXX but we don't want to ever
            // fail... maybe debug_assert.
            assert_eq!(valid_ime_key.keys().secret()
                       .for_storage_encryption().count(), 0);
            assert_eq!(valid_ime_key.keys().secret()
                       .count(), 1);
            assert_eq!(valid_ime_key.userids().count(), 1);

            let mut raw = Vec::new();
            io::stdin().read_to_end(&mut raw)?;
            if let Ok(s) = std::str::from_utf8(&raw) {
                let m = MimeMessage::parse(s)?; // XXX: handle failure
                let m = handle(m, valid_ime_key)?;
                dbg!(&m.headers);
                io::stdout().write_all(m.as_string().as_bytes())?;
                dbg!(&m.headers);
            } else {
                unimplemented!("encrypt and attach malformed mime message");
            }
        },
    }
    Ok(())
}

fn handle(mut m: MimeMessage, ime_key: ValidCert) -> Result<MimeMessage> {
    match m.message_type {
        Some(MimeMultipartType::Encrypted) => Ok(m),
        Some(_) => encrypt(m, ime_key),
        None => {
            let mut part = MimeMessage::new_blank_message();
            part.headers.insert(
                m.headers.get("Content-Type".into())
                    .cloned()
                    .unwrap_or_else(|| email::Header::new("Content-Type".into(),
                                                          "text/plain".into())));
            // XXX: what should the new content type be?
            std::mem::swap(&mut part.body, &mut m.body);
            assert!(m.children.is_empty());
            m.children.push(part);
            encrypt(m, ime_key)
        },
    }
}

fn encrypt(mut m: MimeMessage, ime_key: ValidCert) -> Result<MimeMessage> {
    let mut plaintext = MimeMessage::new_blank_message();
    plaintext.headers.insert(
        m.headers.get("Content-Type".into())
            .cloned()
            .unwrap_or_else(|| email::Header::new("Content-Type".into(),
                                                  "text/plain".into())));
    std::mem::swap(&mut plaintext.children, &mut m.children);
    assert_eq!(m.children.len(), 0);

    // Encrypt the mime container `plaintext`.
    let mut ciphertext = Vec::new();
    let message = Message::new(&mut ciphertext);
    let message = Armorer::new(message).build()?;
    let recipients = ime_key.keys().subkeys()
        .for_storage_encryption();
    let message = Encryptor::for_recipients(message, recipients).build()?;
    let message = Padder::new(message, padme)?;
    let signing_keypair =
        ime_key.keys().subkeys().secret()
        .alive().revoked(false).for_signing()
        .nth(0).unwrap()
        .key().clone().into_keypair()?;
    let message = Signer::new(message, signing_keypair)
        .add_intended_recipient(&ime_key)
        .build()?;
    let mut message = LiteralWriter::new(message).build()?;
    message.write_all(plaintext.as_string().as_bytes())?;
    message.finalize()?;

    // Create the multipart/encrypted structure.
    let ct = email::Header::new(
        "Content-Type".into(),
        format!("multipart/encrypted; \
                 protocol=\"application/pgp-encrypted\"; \
                 boundary=\"{}\"", m.boundary));
    m.headers.replace(ct.clone());
    // XXX work around odd bug in email
    m.headers.insert(ct);

    let mut part_0 = MimeMessage::new_blank_message();
    part_0.headers.insert(
        email::Header::new("Content-Type".into(),
                           "application/pgp-encrypted".into()));
    part_0.body = "Version: 1\r\n".into(); // XXX line ending??
    m.children.push(part_0);
    let mut part_1 = MimeMessage::new_blank_message();
    part_1.headers.insert(
        email::Header::new("Content-Type".into(),
                           "application/octet-stream".into()));
    part_1.body = String::from_utf8(ciphertext)?;
    m.children.push(part_1);
    Ok(m)
}
